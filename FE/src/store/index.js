import Vue from 'vue';
import Vuex from 'vuex';
import books from 'modules/books';
import borrowed from 'modules/borrowed';
import categories from 'modules/categories';
import patrons from 'modules/patrons';
import returned from 'modules/returned';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        books,
        borrowed,
        categories,
        patrons,
        returned,
    }
});