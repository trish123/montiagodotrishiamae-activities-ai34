import axios from "@/config/axios";
import toastr from 'toastr';

export default {
  namespaced: true,

  state: { 
    returned: []
  },

  getters: {},

  mutations: {
    setBorrowedBook: (state, returned) => state.returned.push({ ...returned }),
    setBorrowedBooks: (state, returnedbooks) =>
      (state.returned = returnedbooks),
  },
  
  actions: {
    async returnedBook({ commit }, returned) {
      await axios
        .post("/returnedbooks", returned)
        .then((res) => {
          toastr.success(res.data.message, "Success");
          commit("setRetunedBook", res.data.returned);
        })
        .catch((err) => {
          if (err.response.status == 422) {
            for (var i in err.response.data.errors) {
              toastr.error(err.response.data.errors[i][0], "Failed");
            }
          }
        });
    },
  },
};
