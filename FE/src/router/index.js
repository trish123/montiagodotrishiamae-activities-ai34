import Vue from 'vue'
import VueRouter from 'vue-router'
import Header from '@/components/Header'
import Sidenav from '@/components/Sidenav'
import Dashboard from '@/pages/Dashboard'
import patronmanagement from '@/pages/patronmanagement'
import bookmanagement from '@/pages/bookmanagement'
import settings from '@/pages/settings'


Vue.use(VueRouter)

const routes = [
  {
    path: '/header',
    name: 'Header',
    component: Header,
    props: { page: 0 },
  },
  {
    path: '/sidenav',
    name: 'sidenav',
    component: Sidenav,
    props: { page: 1 },
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    props: { page: 2 },
    alias: '/'
  },
  {
    path: '/patronmanagement',
    name: 'patronmanagement',
    props: { page: 3 },
    component: patronmanagement
  },
  {
    path: '/bookmanagement',
    name: 'bookmanagement',
    props: { page: 4 },
    component: bookmanagement
  },
  {
    path: '/settings',
    name: 'settings',
    props: { page: 5 },
    component: settings
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
