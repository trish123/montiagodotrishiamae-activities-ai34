<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categories=['Technologies', 'Fictions', 'Romance'];

        foreach($categories as $category){
            Category::create(['category'=>$categories]);
        }
    }
}
