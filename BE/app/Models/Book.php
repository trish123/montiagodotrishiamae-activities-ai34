<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'author','copies', 'category_id'];

    //category
    public function category() {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }
    //borrowed books
    public function borrowed() {
        return $this->hasMany(BorrowedBook::class, 'borrowed_books', 'book_id', 'id');
    }

    //returned books
    public function returned() {
        return $this->hasMany(ReturnedBook::class, 'returned_books', 'book_id', 'id');
    }
    
}
