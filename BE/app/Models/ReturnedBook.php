<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnedBook extends Model
{
    use HasFactory;

    protected $fillable = ['book_id', 'copies', 'patron_id'];

    //patron
    public function patron() {
        return $this->belongsTo(Patron::class, 'patron_id', 'id');
    }
    //book
    public function book() {
        return $this->belongsTo(Book::class, 'book_id', 'id');
    }
}
