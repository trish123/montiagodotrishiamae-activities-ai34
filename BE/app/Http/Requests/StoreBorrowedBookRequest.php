<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreBorrowedBookRequest extends FormRequest
{
    /**
     * DETERMINE IF USER IS AUTHORIZE TO MAKE REQUEST
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * GET THE VALIDATION RULES THAT APPLY TO THE REQUEST
     *
     * @return array
     */

    public function rules()
    {
        $book = Book::find(request()->get('book_id'));
            if(!empty($book)){
                $copies = $book->copies;
            }
            else{
                $copies = request()->get('copies');
            }

        return [
            'patron_id' => 'required|',
            'copies' => ['required',"lte: {$copies}", 'bail', 'gt:0'],
            'book_id' => 'required|exists:patrons,id'
        ];
    }

     /**
     * GET THE ERROR MESSAGE FOR THE VALIDATION RULES
     *
     * @return array
     */
    
    public function message()
    {
        return [        
            'patron_id.exists' => 'Patron does not exist',
            'copies' => ['required',"lte: {$copies}", 'bail', 'gt:0'],
            'book_id.exists' => 'Book does not exist'
        ];
    }

    //DISPLAY ERROR MESSAGE
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}