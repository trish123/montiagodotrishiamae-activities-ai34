<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class StorePatronRequest extends FormRequest
{
    /**
     * DETERMINE IF USER IS AUTHORIZE TO MAKE REQUEST
     *
     * @return bool
     */
    
    public function authorize()
    {
        return true;
    }

    /**
     * GET THE VALIDATION RULES THAT APPLY TO THE REQUEST
     *
     * @return array
     */

    public function rules()
    {
        return [
            'last_name' => 'required|min:2|max:100', 
            'first_name' => 'required|min:2|max:100',
            'middle_name' => 'required|min:2|max:100',
            'email' => 'required|unique:patrons|email'
        ];
    }

    /**
     * GET THE ERROR MESSAGE FOR THE VALIDATION RULES
     *
     * @return array
     */

    public function message()
    {
        return [        
            'last_name.required' => 'Last Name is required.',
            'last_name.min' => 'Last Name must have at least minimum of 2 characters',
            'last_name.max' => 'Last Name must not exceed 100 maximum of characters',
            'first_name.required' => 'First Name is required.',
            'first_name.min' => 'First Name must have at least minimum of 2 characters',
            'first_name.max' => 'First Name must not exceed 100 maximum of characters',
            'middle_name.required' => 'Middle Name is required.',
            'middle_name.min' => 'Middle Name must have at least minimum of 2 characters',
            'middle_name.max' => 'Middle Name must not exceed 100 maximum of characters',
            'email.required' => 'Last Name is required.',
            'email.unique' => 'Email already exists',
        ];
    }
    
    //DISPLAY ERROR MESSAGE
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}