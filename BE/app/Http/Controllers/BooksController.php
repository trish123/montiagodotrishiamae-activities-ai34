<?php

namespace App\Http\Controllers;

use app\Models\Book;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    //
    public function index()
    {
        return response()->json(Book::with(['category:id', 'category'])->get());
    }
    public function show($id)
    {
        $books = Book::with(['category:id category'])->where('id', $id)->firsOrFail();
        return response()->json($books);
        $books->update($request->all());
        return response()->json(['book' => $books]);
    }
    public function destroy($id)
    {
        return response()->json(['message', 'Book is Deleted', Book::where('id', $id)->firstOrFail()->delete()]);
    }
}
