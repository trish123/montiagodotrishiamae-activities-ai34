<?php

namespace App\Http\Controllers;

use app\Models\Patron;
use Illuminate\Http\Request;

class PatronController extends Controller
{
    //
    public function index()
    {
        return response()->json(Patron::all());
    }
    public function show($id)
    {
        return response()->json(Patron::findorfail($id));
    }
    public function store(Request $request)
    {
        $patron = Patron::where('id', $id)->firstOrFail();
        $patron->update($request->all());
        return response()->json(['message', 'Patron Updated', 'patron'=>$patron]);
    }
    public function destroy($id)
    {
        return response()->json()->Patron::where('id', $id)->firstOrFail()->delete();
    }
}
