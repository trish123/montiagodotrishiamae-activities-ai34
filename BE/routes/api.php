<?php

use app\Http\Controllers\BookController;
use app\Http\Controllers\BorrowedBookController;
use app\Http\Controllers\CategoryController;
use app\Http\Controllers\PatronController;
use app\Http\Controllers\ReturnedBookController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('patrons', PatronController::class)->only(['index', 'store', 'show', 'update', 'destroy']);
Route::resource('books', BookController::class)->only(['index', 'store', 'show', 'update', 'destroy']);

Route::get('/borrowed', [BorrowedBookController::class, 'index']);
Route::get('/borrowed/{id}', [BorrowedBookController::class, 'show']);
Route::post('/borrowed', [BorrowedBookController::class, 'store']);


Route::get('/categories', [CategoryController::class, 'index']);

Route::get('/returned', [ReturnedBookController::class, 'index']);
Route::get('/returned/{id}', [ReturnedBookController::class, 'show']);
Route::post('/returned', [ReturnedBookController::class, 'store']);
