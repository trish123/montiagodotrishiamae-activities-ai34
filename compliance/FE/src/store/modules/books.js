import axios from "@/config/axios";
import toastr from "toastr";

export default {
  namespaced: true,

  state: {
    books: [],
  },

  getters: {
    getBooks: (state) => state.books,
  },

  mutations: {
    setBooks: (state, books) => (state.books = books),
    setDeleteBook: (state, id) =>
      (state.books = state.books.filter((book) => book.id !== id)),
    setNewBook: (state, book) => state.books.unshift({ ...book }),
    setUpdateBook: (state, updatedBook) => {
      const index = state.books.findIndex((book) => book.id === updatedBook.id);
      if (index !== -1) {
        state.books.splice(index, 1, { ...updatedBook });
      }
    },
  },
  
  actions: {
    async createBook({ commit }, book) {
      await axios
        .post("/books", book)
        .then((res) => {
          commit("setNewBook", res.data.book);
          toastr.success(res.data.message, "Success");
        })
        .catch((err) => {
          if (err.response.status == 422) {
            for (var i in err.response.data.errors) {
              toastr.error(err.response.data.errors[i][0], "Failed");
            }
          }
        });
    },
    async deleteBook({ commit }, id) {
      await axios.delete(`/books/${id}`)
      .then(res => {
        commit("setDeleteBook", id);
        toastr.success(res.data.message, "Success");
      })
      .catch(err => {
        console.error(err.response.data);
      })

    },
    async fetchBooks({ commit }) {
      await axios
        .get("/books")
        .then((res) => {
          commit("setBooks", res.data);
        })
        .catch((err) => {
          console.error(err);
        });
        
    },
    async updateBook({ commit }, book) {

      var id = book.id
      var body = {
        name: book.name,
        author: book.author,
        copies: book.copies,
        category_id: book.category_id
      }

      await axios
        .put(`/books/${id}`, body)
        .then((res) => {
          toastr.success(res.data.message, "Success");
          commit("setUpdateBook", res.data.book);
        })
        .catch((err) => {
          if (err.response.status == 422) {
            for (var i in err.response.data.errors) {
              toastr.error(err.response.data.errors[i][0], "Failed");
            }
          }
        });
    },
  },
};
