import axios from "@/config/axios";
import toastr from 'toastr';

export default {
    namespaced: true,
  
    state: {
      patrons: []
    },

    getters: {
      getPatrons: (state) => state.patrons
    },

    mutations: {
      setPatrons: (state, patrons) => state.patrons = patrons,
      setDeletePatron: (state, id) =>
        (state.patrons = state.patrons.filter((patron) => patron.id !== id)),
      setNewPatron: (state, patron) => state.patrons.unshift({ ...patron }),
      setUpdatePatron: (state, updatedPatron) => {
        const index = state.patrons.findIndex((patron) => patron.id === updatedPatron.id);
        if (index !== -1) {
          state.patrons.splice(index, 1, { ...updatedPatron });
        }
      },
    },
    
    actions: {
      async createPatron({ commit }, patron) {
        await axios
          .post("/patrons", patron)
          .then((res) => {
            commit("setNewPatron", res.data.patron);
            toastr.success(res.data.message, "Success");
          })
          .catch((err) => {
            if (err.response.status == 422) {
              for (var i in err.response.data.errors) {
                toastr.error(err.response.data.errors[i][0], "Failed");
              }
            }
          });
      },
      async deletePatron({ commit }, id) {
        await axios.delete(`/patrons/${id}`)
        .then(res => {
          commit("setDeletePatron", id);
          toastr.success(res.data.message, "Success");
        })
        .catch(err => {
          console.error(err);
        })
  
      },
      async fetchPatrons({ commit }) {
        await axios
          .get("/patrons")
          .then((res) => {
            commit("setPatrons", res.data);
          })
          .catch((err) => {
            console.error(err);
          });
          
      },
      async updatePatron({ commit }, patron) {
  
        var id = patron.id
        var body = {
          last_name: patron.last_name,
          first_name: patron.first_name,
          middle_name: patron.middle_name,
          email: patron.email
        }
  
        await axios
          .put(`/patrons/${id}`, body)
          .then((res) => {
            toastr.success(res.data.message, "Success");
            commit("setUpdatePatron", res.data.patron);
          })
          .catch((err) => {
            if (err.response.status == 422) {
              for (var i in err.response.data.errors) {
                toastr.error(err.response.data.errors[i][0], "Failed");
              }
            }
          });
      },
    },
  };
