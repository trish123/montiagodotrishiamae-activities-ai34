import axios from "@/config/axios";
import toastr from "toastr";

export default {
  namespaced: true,

  state: {
    borrowed: [],
  },
  getters: {
    getBorrowedBooks: (state) => state.borrowed,
  },
  mutations: {
    setBorrowedBook: (state, borrowed) => state.borrowed.push({ ...borrowed }),
    setBorrowedBooks: (state, borrowedBooks) =>
      (state.borrowed = borrowedBooks),
  },
  actions: {
    async borrowedBook({ commit }, borrow) {
      await axios
        .post("/borrowedbooks", borrow)
        .then((res) => {
          toastr.success(res.data.message, "Success");
          commit("setBorrowedBook", res.data.borrowed);
        })
        .catch((err) => {
          if (err.response.status == 422) {
            for (var i in err.response.data.errors) {
              toastr.error(err.response.data.errors[i][0], "Failed");
            }
          }
        });
    },
    async fetchBorrowedBooks({ commit }) {
      await axios
        .get("/borrowedbooks")
        .then((res) => {
          commit("setBorrowedBooks", res.data);
        })
        .catch((err) => {
          console.error(err);
        });
    },
  },
};
