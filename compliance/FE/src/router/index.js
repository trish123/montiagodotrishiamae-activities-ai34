import Vue from 'vue'
import VueRouter from 'vue-router'
import Header from '@/components/Header'
import Sidenav from '@/components/Sidenav'
import Dashboard from '@/pages/Dashboard'
import Room from '@/pages/Room'
import Booking from '@/pages/Booking'
import Categories from '@/pages/Categories'


Vue.use(VueRouter)

const routes = [
  {
    path: '/header',
    name: 'Header',
    component: Header,
    props: { page: 0 },
  },
  {
    path: '/sidenav',
    name: 'sidenav',
    component: Sidenav,
    props: { page: 1 },
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    props: { page: 2 },
    alias: '/'
  },
  {
    path: '/booking',
    name: 'booking',
    props: { page: 3 },
    component: Booking
  },
  {
    path: '/room',
    name: 'room',
    props: { page: 4 },
    component: Room
  },
  {
    path: '/categories',
    name: 'categories',
    props: { page: 5 },
    component: Categories
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
