
/* custom char-data*/
var randomScalingFactor = function(){ return Math.round(Math.random()*550)};
	
		
	var barChartData = {
		labels : ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],
		datasets : [
			{
				
				fillColor : "rgba(220,220,220,0.5)",
				strokeColor : "rgba(220,220,220,0.8)",
				highlightFill: "rgba(220,220,220,0.75)",
				highlightStroke: "rgba(220,220,220,1)",

				data : [180, 300, 220, 540, 401, 500, 160]
			},
			{
				fillColor : "rgba(48, 164, 255, 0.2)",
				strokeColor : "rgba(48, 164, 255, 0.8)",
				highlightFill : "rgba(48, 164, 255, 0.75)",
				highlightStroke : "rgba(48, 164, 255, 1)",
				data : [81, 455, 318, 114, 291, 415, 80]
			}
		]

	}
			
	var doughnutData = [
				{
					value: 350,
					color:"#30a5ff",
					highlight: "#62b9fb",
					label: "Technologies"
				},
				{
					value: 239,
					color: "#ffb53e",
					highlight: "#fac878",
					label: "Discoveries"
				},
				
				{
					value: 158,
					color: "#f9243f",
					highlight: "#f6495f",
					label: "Realities"
				}

			];
			